<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Widgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('package', function (Blueprint $table) {
           $table->id();
           $table->double('size');
           $table->integer('xs');
           $table->integer('s');
           $table->integer('m');
           $table->integer('l');
           $table->integer('xl');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('package');
    }
}
