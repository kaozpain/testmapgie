<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{packageId?}', 'App\Http\Controllers\mainController@index')->name('home');
Route::post('/package/save', 'App\Http\Controllers\mainController@savePackage')->name('savePackage');
Route::get('/calculate/{value}', 'App\Http\Controllers\mainController@calculateWidgets')->name('calculate');

//Route::get('/', function () {
//    return view('welcome');
//});
