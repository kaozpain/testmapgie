let main = {
    validate: function (input, key) {
        let value = input.value;
        let numberKey = /^[0-9]*$/;
        let pointKey = /^\.*$/;
        let pointExists = value.includes('.');

        if (pointExists)
            if (value.indexOf('.') == 0)
                input.value = "0"+value;

        if (numberKey.test(key) || (pointKey.test(key) && !pointExists)) {
            return true;
        } else {
            return false;
        }
    },
    calculate: function (value) {
        console.log(root);
        main.restartCounters();
        $.ajax({
            url: root + '/calculate/' + value,
            type: "GET",
        }).done(function (r) {
            main.updateCounters(r);
        }).fail(function (r) {
            console.log(r);
        })
    },
    restartCounters: function () {
        $('#XS')[0].innerText = 0;
        $('#S')[0].innerText = 0;
        $('#M')[0].innerText = 0;
        $('#L')[0].innerText = 0;
        $('#XL')[0].innerText = 0;
    },
    updateCounters: function (results) {
        let result = JSON.parse(results);
        Object.keys(result).map( function (key) {
            $('#'+key)[0].innerText = result[key];
        });
    }
};
