<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use function PHPSTORM_META\map;

class mainController extends Controller
{
    static $size = [0 => 'XS', 1 => 'S', 2 => 'M', 3 => 'L' , 4 => 'XL'];
    static $widgets = [250, 500, 1000, 2000, 5000];

    /**
     * Prints the initial page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($packageId = null) {
        $data['root'] = route('home');
        $data['Controller'] = $this;
        if (is_null($packageId))
            return view('main', $data);
        else {
            /** @var Package $Package */
            $Package = Package::find($packageId);
            if (!$Package)
                return view('main');
            $data['packageId'] = $Package->id;
            $data['size'] = $Package->size;
            $data['xs'] = $Package->xs;
            $data['s'] = $Package->s;
            $data['m'] = $Package->m;
            $data['l'] = $Package->l;
            $data['xl'] = $Package->xl;
            return view('main', $data);
        }
    }

    public function tableData() {
        $Packages = Package::all();
        $data['Packages'] = $Packages;
        return view('tableData', $data);
    }

    public function savePackage() {
        $widgets = static::$widgets;
        arsort($widgets);
        $usedWidgets = $this->getNeccesaryWidgets($_POST['size'], $widgets);
        if (isset($_POST['packageId'])) {
            /** @var Package $Package */
            $Package = Package::find($_POST['packageId']);
        } else {
            $Package = new Package();
        }
        $Package->size = $_POST['size'];
        $Package->xs = $usedWidgets['XS'];
        $Package->s = $usedWidgets['S'];
        $Package->m = $usedWidgets['M'];
        $Package->l = $usedWidgets['L'];
        $Package->Xl = $usedWidgets['XL'];
        $Package->save();
        return redirect('/');
    }

    /**
     * Responds the package and widget details
     *
     * @param $packageSize
     */
    public function calculateWidgets($packageSize) {
        $widgets = static::$widgets;
        arsort($widgets);
        $usedWidgets = $this->getNeccesaryWidgets($packageSize, $widgets);
        header('Content-type: application/json');
        echo json_encode($usedWidgets);
    }

    /**
     * Widgets Logic, gets the neccesary widgets for the package
     *
     * @param $packageSize
     * @param $widgets
     * @return mixed
     */
    public function getNeccesaryWidgets($packageSize, $widgets) {
        foreach ($widgets as $key => $widget) {
            $widgetCounter = $key == 0 ? ceil($packageSize / $widget) : floor($packageSize / $widget);
            $packageSize = fmod($packageSize, $widget);
            if ($key > 0 && ($packageSize / $widgets[$key-1]) > 1 && fmod($packageSize, $widgets[$key-1]) > 0) {
                $lostPackage = $widget - $packageSize;
                $correctPackage = true;
                $sumCombination = $key > 1 ? $this->getSumCombinations($key-1): [];
                foreach ($widgets as $tmpWidget) {
                    foreach ($sumCombination as $combination)
                        if ($combination > $lostPackage && $combination >= $packageSize)
                            $correctPackage = false;
                    if ($tmpWidget < $lostPackage)
                        $correctPackage = false;
                }
                if ($correctPackage) {
                    $widgetCounter++;
                    $packageSize = 0;
                }
            }
            $requiredWidgets[static::$size[$key]] = $widgetCounter;
        }
        return $requiredWidgets;
    }

    public function getSumCombinations($to) {
        $widgets = static::$widgets;
        $sumAll = 0;
        for ($count = 0; $count <= $to; $count++) {
            $sumAll += $widgets[$count];
            $sum[$count] = $sumAll;
        }
        return $sum?:[];
    }
}
