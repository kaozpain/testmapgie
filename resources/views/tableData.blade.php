<table style="border: dashed;">
    <tr>
        <th>Id</th>
        <th>Size</th>
        <th>XS Packages</th>
        <th>S Packages</th>
        <th>M Packages</th>
        <th>L Packages</th>
        <th>XL Packages</th>
        <th>Actions</th>
    </tr>
    @foreach($Packages as $Package)
        <tr>
            <td>{{ $Package->id }}</td>
            <td>{{ $Package->size }}</td>
            <td>{{ $Package->xs }}</td>
            <td>{{ $Package->s }}</td>
            <td>{{ $Package->m }}</td>
            <td>{{ $Package->l }}</td>
            <td>{{ $Package->xl }}</td>
            <td>
                <a href="{{ route('home').'/'.$Package->id }}">Edit</a>
            </td>
        </tr>
    @endforeach
</table>
