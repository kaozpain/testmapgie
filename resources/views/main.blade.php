<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test Mapgie</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
</head>
<body>
<h1>Welcome to your Widget calculator Page (automatic)</h1>
@isset($packageId)
    <h1><a href="{{ route('home') }}">Click here to add a new Package</a> </h1>
@endisset
<div class="regularInput">
    <form method="post" action="{{ route('savePackage') }}" enctype="multipart/form-data">
        @csrf
        @isset($packageId)
            <h1> Editing package number: {{ $packageId }} </h1>
        @else
            <h1> New package </h1>
        @endisset
        <strong id="inputText">Please write the package size: </strong>
        @isset($packageId)
            <input name="packageId" type="hidden" value="{{ $packageId }}">
        @endisset
        <input placeholder="Just Int or Double numbers" maxlength="22" onkeypress="return main.validate(this, event.key)" onkeyup="main.calculate(this.value)" type="text" name="size" value="@isset($size){{ $size }}@endisset">
        <button type="submit">Save</button>
    </form>
</div>
<div class="BackToNormal">
    <p>Used Widgets XS (250): <a id="XS">@isset($xs) {{ $xs }} @else 0 @endisset</a></p>
    <p>Used Widgets S (500): <a id="S">@isset($s) {{ $s }} @else 0 @endisset</a></p>
    <p>Used Widgets M (1000): <a id="M">@isset($m) {{ $m }} @else 0 @endisset</a></p>
    <p>Used Widgets L (2000): <a id="L">@isset($l) {{ $l }} @else 0 @endisset</a></p>
    <p>Used Widgets XL (5000): <a id="XL">@isset($xl) {{ $xl }} @else 0 @endisset</a></p>
</div>
<div>
    {{ $Controller->tableData() }}
</div>
<script>
    let root = "{{ $root }}";
</script>
<script src="{{ asset('/js/main.js') }}"></script>
<script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
</body>
</html>
